import 'package:chat_app/ChatMessages.dart';
import 'package:flutter/material.dart';

class ChatView extends StatefulWidget {
  @override
  _ChatViewState createState() => _ChatViewState();

  final String freindName;

  final String lastMessage;

  ChatView({Key key, this.freindName, this.lastMessage}) : super(key: key);
}

class _ChatViewState extends State<ChatView> {
  String _freindInitial;

  TextEditingController _controller=TextEditingController();

  @override
  void initState() {
    setState(() {
      _freindInitial = widget.freindName.substring(0, 1);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.freindName),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
                      child: ListView(
              children: <Widget>[
                ChatMessages(
                  isFriend: true,
                  isNotPrevious: true,
                  freindInitial: _freindInitial,
                  message: "I really want to SNAP",
                ),
                // ChatMessages(
                //   isFriend: false,
                //   isNotPrevious: true,
                //   freindInitial: _freindInitial,
                // ),
              ],
            ),
          ),//Flexible

Padding(
  padding:  EdgeInsets.all(16.0),
  child:   Row(

    crossAxisAlignment: CrossAxisAlignment.center,
  
              children: <Widget>[
  
                Expanded(child: TextFormField(
  
  controller: _controller,

  onFieldSubmitted: (String _message){

print("on field submitted >> "+_message);

  },
  
  decoration: InputDecoration(hintText: "Type Your Text Here.."
  
  ,labelText: "Your Message",
  
  helperText: "Here's Where the message goes"
  
  ),
  
                  
  
                )),
  
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: IconButton(
  
                      icon: Icon(Icons.send
                      ,color: Colors.blue,),
                      
  
                      onPressed: () {
  
                        print("send message tapped>> "+_controller.text);
  
                      },
  
                    ),
                  ),
                )
  
              ],
  
            ),
),


        ],
      ),

     

// floatingActionButton: FloatingActionButton(onPressed: null,child: Icon(Icons.add)),
    );
  }
}
