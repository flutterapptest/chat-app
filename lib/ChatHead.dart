import 'package:chat_app/ChatView.dart';
import 'package:flutter/material.dart';

class ChatHead extends StatefulWidget {
  @override
  _ChatHeadState createState() => _ChatHeadState();

  final String freindsName;

  final String lastMessage;

  final DateTime messageTime;

  ChatHead(
      {Key key, this.freindsName = "", this.lastMessage = "", this.messageTime})
      : super(key: key);
}

class _ChatHeadState extends State<ChatHead> {
  @override
  Widget build(BuildContext context) {
    return InkWell(

      onTap: ()async{

        print(widget.freindsName+ " has tapped");

        await Navigator.of(context).push(MaterialPageRoute<Null>(

          builder: (BuildContext context){

            return ChatView(freindName: widget.freindsName,
            lastMessage: "Snap",);
          },
          fullscreenDialog: true)
        );
      },

      highlightColor: Colors.blue,
          child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          width: double.infinity,
          height: 100.0,
        //  color: Colors.grey[300],
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(widget.freindsName,
                        style: Theme.of(context).textTheme.headline6),
                    Text(widget.lastMessage,
                        style: Theme.of(context).textTheme.headline6),
                    Text(widget.messageTime.toString(), style: Theme.of(context).textTheme.headline6),
                  ],
                ),
              ),

              CircleAvatar(
                radius: 28,
                child: Text(
                  widget.freindsName.substring(0,1),
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .apply(color: Colors.white, fontSizeDelta: 3),
                ),
              ),
              //)
            ],
          )),
    );
  }
}
